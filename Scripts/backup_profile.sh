#!/bin/bash

PROFILE_PATH=/media/fat/dotfiles
mkdir -p "$PROFILE_PATH"

cp ~/.profile "$PROFILE_PATH"
cp ~/.bash_{history,aliases} "$PROFILE_PATH"
cp -Rp ~/.ssh/ "$PROFILE_PATH"
