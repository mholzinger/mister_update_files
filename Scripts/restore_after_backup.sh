#!/bin/bash

# Copy back ssh keys
cp /media/fat/dotfiles/.ssh/authorized_keys ~/.ssh/
chmod 600 ~/.ssh/authorized_keys
chmod 700 ~/.ssh

# Copy back our profiles
cp /media/fat/dotfiles/.profile ~/
cp /media/fat/dotfiles/.bash_* ~/

