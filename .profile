export PS1='\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w \$\[\033[00m\] '
export TERM=xterm
export LC_CTYPE=en_US.UTF-8

cd /media/fat

# Define your own aliases here ...
if [ -f ~/.bash_aliases ]; then
      . ~/.bash_aliases
fi

print_mister_ver(){
  CORENAME=$(cat /var/log/CORENAME)
  MISTER_VER=$(grep -s '$VER' /media/fat/MiSTer|cut -d':' -f2)
  source /etc/os-release
  echo "MiSTer ver: $MISTER_VER | OS: $PRETTY_NAME | Loaded core: $CORENAME"
}

print_mister_ver
echo

# Print MOTD
echo "Available filespace:"
# Testing for the mounted device id until we hit it, print it and then bailing
for device in $(seq 0 9)
do
  [ -d "/media/usb${device}/games" ] && \
  df -h /media/{fat,usb${device}} && \
  break
done
