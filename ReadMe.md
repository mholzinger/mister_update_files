##Breakdown of the update scripts I am using on my personal MiSTer builds.


**First note:**

I've written some profile backup scripts that I use to make backed up ssh and profile settings for mister linux full os upgrades, which typically blow away ssh settings by removing .ssh/, .profile, .bashrc and .bash_aliases files.

Those backup files (saved here in `./Scripts/`) create a backup folder at `/media/fat/dotfiles/`

Restoring can be done by calling `Scripts/restore_after_backup.sh`

In `.profile here`, I'm calling a command fron the MiSTer by sshing in and running `$ doupdate` which calls, backup, the jotego updater and then the official mister updater.

****

I'm using three scripts to update:

The official Mister updater:
[Updater_script_MiSTer](https://github.com/MiSTer-devel/Updater_script_MiSTer)

The older Jotego Updater:
[jtupdate](https://github.com/jotego/jtupdate)

The less reliable but more popular update_all script:
[Update_All_MiSTer](https://github.com/theypsilon/Update_All_MiSTer)

My config files are probably what are more worth reviewing for folks who already have their own custom update configs.

```
Scripts/update.ini
Scripts/update_all.ini
```

****

How I'm using these:

- Typically I always call my bash alias command `doupdate` first. It calls the official updater script and for me, I feel does a much more stable and reliiable job than the update_all script.

- When I see new mra files: I'll run the update_all script from the terminal so that I can invoke mame-getter.

That's it!

Please PR this repo if you see some older ini file entries that don't belong here.


